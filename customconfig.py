#!/usr/bin/python

import os, sys, platform
import json
import subprocess
import stat
import mimetypes
import shutil
import re

class customconfig:
    """ Config Linux Servers
    """

    def __init__(self, in_file = None):
	""" try to find system info
	"""
	if in_file is None:
	    raise Exception("Please specify job json!")

	if platform.system() != 'Linux':
	    raise Exception("Insuported system!")

	( self.os_name, self.os_version, self.os_extra ) = platform.dist()
	self.app_config = self.app_config_load()
	self.tasks = self.load_tasks(in_file)
	self.packages_installed = []
	self.files_copied = []
	self.packages_uninstalled = []

    def start(self):
	""" start parsing jobs and execute
	"""
	if "tasks" not in self.tasks:
	    raise Exception("No tasks key found!")
	for task in self.tasks["tasks"]:
	    if "module" in task:
		if task["module"] == "install":
		    self.install_module(task)
		elif task["module"] == "uninstall":
		    self.uninstall_module(task)
		elif task["module"] == "copy_file":
		    self.copy_file_module(task)
		elif task["module"] == "delete_file":
		    self.delete_file_module(task)
		elif task["module"] == "service":
		    self.service_module(task)
		else:
		    raise Exception("Invalid module name: " + task["module"])
	    else:
		raise Exception("No module specified!")
	return True

    def get_os_command(self, command):
	""" get os commands template
	"""
	for os_name in self.app_config["OS"]:
	    if os_name == self.os_name.lower():
		if command in self.app_config["OS"][os_name]:
		    return self.app_config["OS"][os_name][command]
		else:
		    raise Exception("Unknown command " + command + "for " + os_name)
	raise Exception("Unsuported OS!" + self.os_name)

    def service_module(self, task):
	""" Service admin module
	"""
	if "description" in task:
	    print(task["description"])
	if "name" not in task:
	    raise Exception("You must specify service name")
	if "enabled" in task and task["enabled"] == "yes":
	    try:
		cmd = self.get_os_command("service_enable").replace("<service_name>", task["name"])
		self.run_command(cmd)
	    except Exception, e:
		raise Exception(repr(e))
	if "config" in task:
	    # config service
	    print("To Do")
	if "state" in task:
	    if task["state"] == "started":
		try:
		    cmd = self.get_os_command("service_start").replace("<service_name>", task["name"])
		    self.run_command(cmd)
		    print("Started service " + task["name"] + " successfully")
		except Exception, e:
		    raise Exception("Unabled to start service " + task["name"] + " " + repr(e))
	    elif task["state"] == "stopped":
		try:
		    cmd = self.get_os_command("service_stop").replace("<service_name>", task["name"])
		    self.run_command(cmd)
		    print("Stopped service " + task["name"] + " successfully")
		except Exception, e:
		    raise Exception("Unabled to stop service " + task["name"] + " " + repr(e))
	    elif task["state"] == "restarted":
		try:
		    cmd = self.get_os_command("service_restart").replace("<service_name>", task["name"])
		    self.run_command(cmd)
		    print("Restarted service " + task["name"] + " successfully")
		except Exception, e:
		    raise Exception("Unabled to restart service " + task["name"] + " " + repr(e))
	    elif task["state"] == "reloaded":
		try:
		    cmd = self.get_os_command("service_reload").replace("<service_name>", task["name"])
		    self.run_command(cmd)
		    print("Reloaded service " + task["name"] + " successfully")
		except Exception, e:
		    raise Exception("Unabled to start reload " + task["name"] + " " + repr(e))
	    else:
		raise Exception("Unknown state")

    def summary(self):
	""" print task summary
	"""
	if len(self.packages_installed) > 0:
	    print("Installed packages:")
	    for pkg in self.packages_installed:
		print("\t" + pkg)
	if len(self.packages_uninstalled) > 0:
	    print("Uninstalled packages:")
	    for pkg in self.packages_uninstalled:
		print("\t" + pkg)
	if len(self.files_copied) > 0:
	    print("Files copied:")
	    for filec in self.files_copied:
		print("\t" + filec)


    def uninstall_module(self, task):
	""" uninstall package module
	"""
	if "description" in task:
	    print(task["description"])
	if "package" not in task:
	    raise Exception("No package specified")
	cmd = self.get_uninstall_command().replace("<package_name>", task["package"])
	if self.run_command(cmd):
	    print("Successfull")
	    self.packages_uninstalled.append(task["package"].split())
	else:
	    raise Exception("Failed!")

    def get_uninstall_command(self):
	""" get install command template
	"""
	for os_name in self.app_config["OS"]:
	    if os_name == self.os_name.lower():
		return self.app_config["OS"][os_name]["uninstall"]
	raise Exception("Unsuported OS!" + self.os_name)


    def delete_file_module(self, task):
	""" delete files from the system
	"""
	if "description" in task:
	    print(task["description"])
	if "mode" in task and task["mode"] == "rmtree":
	    shutil.rmtree(task["target"])
	    return True
	try:
	    os.remove(task["target"])
	except Exception, e:
	    raise Exception("Unable to delete file " + task["target"] + ": " + repr(e))

    def copy_file_module(self, task):
	""" copy file module
	"""
	if "description" in task:
	    print(task["description"])
	if "src" in task and "dst" in task:
	    # simple copy file
	    try:
		self.copy_file(task["src"], task["dst"], task.get("meta"), task.get("tags"))
	    except Exception, e:
		print("WARNING! Unable to copy file " + task["src"] + " to " + task["dst"] + " - " + repr(e))
	elif "src_template" in task:
	    ### to do ... find files in src_template and copy them
	    raise Exception("Work in progress")
	else:
	    raise Exception("Invalid module options")

    def copy_file(self, src, dst, meta = None, tags = None):
	""" copy_file
	"""
	if os.path.exists(src):
	    # TODO: backup dst file? 
	    if os.path.exists(dst):
		dst_fname = os.path.basename(dst)
		cmd = "cp -f " + str(dst) + " " + str(os.path.split(dst)[0]) + "/confbkp." +  str(os.path.basename(dst))
		try:
		    self.run_command(cmd)
		except Exception, e:
		    print("WARNING! Unabled to create backup file for " + dst + ": " + cmd + " error: " +  repr(e))
	    # check dst folder
	    if not os.path.exists(os.path.split(dst)[0]):
		os.makedirs(os.path.split(dst)[0])
	    if meta is None:
		meta = {}
		meta["permissions"] = self.get_file_meta(src, "permissions")
		meta["owner"] = self.get_file_meta(src, "owner")
	    else:
		if "permissions" not in meta:
		    meta["permissions"] = self.get_file_meta(src, "permissions")
		if "owner" not in meta:
		    meta["owner"] = self.get_file_meta(src, "owner")
	    # if file is text and we have tags try to replace them
	    if self.check_file_text(src) and tags is not None:
		# parsing file line by line and replace tags
		self.copy_file_tags(src, dst, meta, tags)
	    else:
		cmd = "cp " + src + " " + dst
		try:
		    self.run_command(cmd)
		except Exception, e:
		    raise Exception("Unable to copy to destination file")
		self.file_apply_meta(dst, meta)
		self.files_copied.append(dst)

	else:
	    raise Exception("src file not found")

    def copy_file_tags(self, src, dst, meta, tags):
	""" generate a temporary file and
	    copy to dst
	"""
	try:
	    tmp_file = "/tmp/" + os.path.basename(src)
	    with open(src, 'r') as f:
		src_content = f.readlines()
	    with open(tmp_file, 'w+') as d:
		for line in src_content:
		    pattern = re.compile(r'.*(<(\S+)>)', re.IGNORECASE)
		    findCtrl = True
		    while findCtrl:
			m = pattern.findall(line)
			if len(m) == 0:
			    findCtrl = False
			    continue
			( tg_str, tg_name ) = m[0]
			if tags.get(tg_name) is None:
			    print("WARNING: " + tg_name + " is no defined!")
			line = line.replace(tg_str, tags.get(tg_name,"!novalue!"))
		    d.write(line)
	    self.copy_file(tmp_file, dst, meta, None)

	except Exception,e:
	    raise Exception("Cannot generate file " + repr(e))


    def file_apply_meta(self, filename, meta):
	""" Apply owner and permissions to file
	"""
	cmd = "chown " + meta["owner"] + " " + filename
	try:
	    self.run_command(cmd)
	except Exception, e:
	    raise Exception("Unable to run command " + cmd)
	cmd = "chmod " + meta["permissions"] + " " + filename
	try:
	    self.run_command(cmd)
	except Exception, e:
	    raise Exception("Unable to run command " + cmd)
	return True


    def check_file_text(self, filename):
	""" check is file is text or binary
	"""
	try:
	    cmd = "file " + filename
	    out = subprocess.check_output(cmd.split() )
	    if "ASCII" in out:
		return True
	    return False
	except Exception, e:
	    return False


    def get_file_meta(self, file_path, meta = "permissions"):
	""" return file meta
	"""
	if meta == "owner":
	    return str(os.stat(file_path).st_uid) + ":" + str(os.stat(file_path).st_gid)
	elif meta == "permissions":
	    return oct(stat.S_IMODE(os.stat(file_path).st_mode))
	else:
	    raise Exception("Invalid meta type")


    def install_module(self, task):
	""" install package module
	"""
	if "description" in task:
	    print(task["description"])
	if "package" not in task:
	    raise Exception("No package specified")
	cmd = self.get_install_command().replace("<package_name>", task["package"])
	if self.run_command(cmd):
	    for pkg in task["package"].split():
		self.packages_installed.append(pkg)
	    print("Successfull")
	else:
	    raise Exception("Failed!")

    def run_command(self, command):
	""" exec command
	"""
	try:
	    # multiple commands?
	    for cmd in command.split("&&"):
		subprocess.check_call(cmd.split(), stdout=open(os.devnull, 'w'), stderr=subprocess.STDOUT)
	    return True
	except subprocess.CalledProcessError, e:
	    raise Exception("Command " + command + " failed!")
	except Exception, e:
	    raise Exception("Unable to run command " + command + " error: " + repr(e))


    def get_install_command(self):
	""" get install command template
	"""
	for os_name in self.app_config["OS"]:
	    if os_name == self.os_name.lower():
		return self.app_config["OS"][os_name]["install"]
	raise Exception("Unsuported OS!" + self.os_name)

    def app_config_load(self):
	""" application data
	"""
	try:
	    with open("customconfig.conf") as f:
		return json.load(f)
	except Exception, e:
	    raise Exception("Unable to read config file customconfig.conf " + repr(e))

    def load_tasks(self, in_file):
	""" read from file to dictionary
	"""
	try:
	    with open(in_file) as f:
		return json.load(f)
	except Exception, e:
	    raise Exception("Unable to read from " + in_file + repr(e))
