#!/usr/bin/python

import sys, getopt, os
import customconfig


if __name__ == "__main__":

    lock_file = "/var/lock/config.lock"
    in_file = None
    opts, args = getopt.getopt(sys.argv[1:], "f:h")
    for opt in opts:
	(opt_name, opt_value) = opt
	if opt_name == "-f":
	    in_file = opt_value

    if in_file is None:
	print("Usage: ./config.py -f <filename.json>")
	sys.exit(1)
    # check lock file
    if os.path.exists(lock_file):
	print("Config in already running")
	sys.exit(1)
    with open(lock_file, 'w+') as f:
	f.write(str(os.getpid()))
    try:
	obj = customconfig.customconfig(in_file)
	obj.start()
	obj.summary()
    except Exception, e:
	print("Exception: " + repr(e))
    os.remove(lock_file)


"""
try:
    obj = customconfig.customconfig("lap.json");
    obj.start()
    print(obj.__dict__)
except Exception, e:
    print(repr(e))
"""